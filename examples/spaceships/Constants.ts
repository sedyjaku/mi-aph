// alias for intro
export const INTRO = "intro";


// message keys
export const MSG_GAME_STARTED = "GAME_STARTED";
export const MSG_PROJECTILE_SHOT = "projectile_shot";
export const MSG_GAME_OVER = "game_over";
export const MSG_PLAYER_COLLISION = "enemy_collision";
export const MSG_ENEMY_COLLISION = "player_collision";
export const MSG_ENEMY_CREATED = "enemy_created";
export const MSG_ANIM_ENDED = "anim_ended";
export const MSG_UNIT_KILLED = "unit_killed";
export const MSG_ENEMY_PROJECTILE_SHOT = "enemy_projectile_shot";
export const MSG_NEXT_LEVEL = "next_level";
export const MSG_GAME_FINISHED = "game_finished";
export const MSG_LEVEL_FINISHED = "lvl_finished";



// sound aliases
export const SOUND_FIRE = "fire";
export const SOUND_GAMEOVER = "gameover";
export const SOUND_KILL = "kill";

// attribute keys
export const ATTR_MODEL = "MODEL";
export const ATTR_FACTORY = "FACTORY";

// flags
export const FLAG_PROJECTILE_PLAYER = 1;
export const FLAG_PROJECTILE_ENEMY = 4;
export const FLAG_COLLIDABLE_BY_PLAYER = 2;
export const FLAG_COLLIDABLE_BY_ENEMY = 3;

// alias for texture
export const TEXTURE_SPACESHIP = "spaceship";
export const TEXTURE_SPACESHIPS = "spaceships";
export const TEXTURE_ENEMY = "enemy";
export const TEXTURE_SHOT_1 = "shot_1";
export const TEXTURE_SHOT_2 = "shot_2";
export const TEXTURE_SHOT_3 = "shot_3";
export const TEXTURE_SHOT_4 = "shot_4";
export const TEXTURE_SHOT_ENEMY = "shot_4";

// alias for config file
export const DATA_JSON = "DATA_JSON";

// tags for game objects
export const TAG_SPACESHIP = "spaceship";
export const TAG_PROJECTILE = "projectile";
export const TAG_ENEMY = "enemy";
export const TAG_BACKGROUND = "background";
export const TAG_GAME_OVER = "gameover";
export const TAG_SCORE = "score";
export const TAG_LEVEL = "level";
export const TAG_LIVES = "lives";
export const TAG_LEVEL_ONE = "lvl1";


// height of the scene will be set to 25 units for the purpose of better calculations
export const SCENE_HEIGHT = 25;

// native height of the game canvas. If bigger, it will be resized accordingly
export const SPRITES_RESOLUTION_HEIGHT = 400;

// native speed of the game
export const GAME_SPEED = 1;;


///// SPACESHIP UP
// message keys