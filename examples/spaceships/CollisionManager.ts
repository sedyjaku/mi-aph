/**
 * Entity that keeps info about a collision
 */
import {PIXICmp} from "../../ts/engine/PIXIObject";
import {SpaceshipsBaseCmp} from "./ShaceshipsBaseCmp";
import {MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED} from "../../ts/engine/Constants";
import Msg from "../../ts/engine/Msg";
import {
    FLAG_COLLIDABLE_BY_ENEMY,
    FLAG_COLLIDABLE_BY_PLAYER, FLAG_PROJECTILE_ENEMY,
    FLAG_PROJECTILE_PLAYER, MSG_ENEMY_COLLISION, MSG_PLAYER_COLLISION
} from "./Constants";

export class CollisionInfo {
    // hit unit
    unit: PIXICmp.ComponentObject;
    // projectile that hit given unit
    projectile: PIXICmp.ComponentObject;

    constructor(unit: PIXICmp.ComponentObject, projectile: PIXICmp.ComponentObject) {
        this.unit = unit;
        this.projectile = projectile;
    }
}

/**
 * Simple collision manager
 */
export class CollisionManager extends SpaceshipsBaseCmp {
    enemyUnits = new Array<PIXICmp.ComponentObject>();
    playerUnits = new Array<PIXICmp.ComponentObject>();
    playerProjectiles = new Array<PIXICmp.ComponentObject>();
    enemyProjectiles = new Array<PIXICmp.ComponentObject>();

    onInit() {
        super.onInit();
        this.subscribe(MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED);
    }

    /**
     * adds message actions
     * @param msg
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_ADDED || msg.action == MSG_OBJECT_REMOVED) {
            // refresh collections
            this.playerProjectiles = this.scene.findAllObjectsByFlag(FLAG_PROJECTILE_PLAYER);
            this.enemyProjectiles = this.scene.findAllObjectsByFlag(FLAG_PROJECTILE_ENEMY);
            this.playerUnits = this.scene.findAllObjectsByFlag(FLAG_COLLIDABLE_BY_ENEMY);
            this.enemyUnits = this.scene.findAllObjectsByFlag(FLAG_COLLIDABLE_BY_PLAYER);
        }
    }

    /**
     * checks colision for enemies with player projectile and vice versa
     */
    onUpdate(delta, absolute) {
        let playerCollisions = new Array<CollisionInfo>();
        let enemyCollisions = new Array<CollisionInfo>();

        // O(m^n), we don't suppose there will be more than 50 units in total
        for (let projectile of this.enemyProjectiles) {
            for (let unit of this.playerUnits) {
                let boundsA = projectile.getPixiObj().getBounds();
                let boundsB = unit.getPixiObj().getBounds();

                let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                let intersectionY = this.testVertIntersection(boundsA, boundsB);

                if (intersectionX > 0 && intersectionY > 0) {
                    // we have a collision
                    playerCollisions.push(new CollisionInfo(unit, projectile));
                }
                ;

            }
        }
        for (let projectile of this.playerProjectiles) {
            for (let unit of this.enemyUnits) {
                let boundsA = projectile.getPixiObj().getBounds();
                let boundsB = unit.getPixiObj().getBounds();

                let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                let intersectionY = this.testVertIntersection(boundsA, boundsB);

                if (intersectionX > 0 && intersectionY > 0) {
                    // we have a collision
                    enemyCollisions.push(new CollisionInfo(unit, projectile));
                }
                ;

            }
        }
        // send message for all colliding objects
        for (let collid of enemyCollisions) {
            this.sendMessage(MSG_ENEMY_COLLISION, collid);
        }// send message for all colliding objects
        for (let collid of playerCollisions) {
            this.sendMessage(MSG_PLAYER_COLLISION, collid);
        }
    }

    /**
     * Checks horizontal intersection
     */
    private testHorizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    /**
     * Checks vertical intersection
     */
    private testVertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }
}