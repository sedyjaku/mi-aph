/**
 * Collision resolver component
 */
import {SpaceshipsBaseCmp} from "./ShaceshipsBaseCmp";
import Msg from "../../ts/engine/Msg";
import {
    MSG_ENEMY_COLLISION, MSG_GAME_OVER, MSG_LEVEL_FINISHED,
    MSG_PLAYER_COLLISION,
    MSG_UNIT_KILLED,
    TAG_ENEMY,
    TAG_PROJECTILE,
    TAG_SPACESHIP
} from "./Constants";
import {CollisionInfo} from "./CollisionManager";


export class CollisionResolver extends SpaceshipsBaseCmp {

    onInit() {
        super.onInit();
        this.subscribe(MSG_ENEMY_COLLISION);
        this.subscribe(MSG_PLAYER_COLLISION);
    }

    onMessage(msg: Msg) {
        if (this.model.isGameOver) {
            return;
        }

        if (msg.action == MSG_ENEMY_COLLISION) {
            this.handleEnemyCollision(msg);
        }
        if (msg.action == MSG_PLAYER_COLLISION) {
            this.handlePlayerCollision(msg);
        }
    }


    /**
     * Resolves which enemy was hit and sends level finished message if score reached was enough
     */
    private handleEnemyCollision(msg: Msg) {
        let trigger = <CollisionInfo>msg.data;
        if(trigger.unit.getTag() == TAG_ENEMY){
            this.model.score += this.model.enemyReward;
            this.sendMessage(MSG_UNIT_KILLED, trigger.unit);
            if(this.model.scoreToNext <= this.model.score){
                this.sendMessage(MSG_LEVEL_FINISHED);
            }
        }
        else if( trigger.unit.getTag() == TAG_PROJECTILE){
            this.sendMessage(MSG_UNIT_KILLED, trigger.unit);
        }
        trigger.unit.remove();
        trigger.projectile.remove();

    }

    /**
     * resolves player being hit and and sends game over message if player died
     */
    private handlePlayerCollision(msg: Msg) {

        let trigger = <CollisionInfo>msg.data;
        if(trigger.unit.getTag() == TAG_SPACESHIP){
            this.model.lives--;
            if (this.model.lives === 0){
                trigger.unit.remove();
                this.sendMessage(MSG_GAME_OVER);
                //this.gameOver()
            }
            this.sendMessage(MSG_UNIT_KILLED, trigger.unit);
        }
        else if( trigger.unit.getTag() == TAG_PROJECTILE){
            this.sendMessage(MSG_UNIT_KILLED, trigger.unit);
        }
        trigger.projectile.remove();
    }

}