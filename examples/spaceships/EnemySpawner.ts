/**
 * Global component responsible for creating new copters
 */
import {SpaceshipsBaseCmp} from "./ShaceshipsBaseCmp";
import Msg from "../../ts/engine/Msg";
import {MSG_ENEMY_CREATED, MSG_LEVEL_FINISHED, MSG_UNIT_KILLED, TAG_ENEMY} from "./Constants";
import {checkTime} from "./Utilts";


export class EnemySpawner extends SpaceshipsBaseCmp {
    lastSpawnTime = 0;
    spawnFrequency = 1000;

    onInit() {
        super.onInit();
        this.subscribe(MSG_UNIT_KILLED);
        this.spawnFrequency = this.model.enemySpawnFrequency;
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_UNIT_KILLED && msg.gameObject.proxy.tag == TAG_ENEMY) {
            // sync number of copters on the scene
            this.model.enemiesCreated--;
        }
    }

    /**
     * Spawns enemies
     */
    onUpdate(delta, absolute) {
        if (checkTime(this.lastSpawnTime, absolute, this.spawnFrequency)) {
            // create a new enemy spaceship and speed up spawn frequency
            this.model.enemiesCreated++;
            this.lastSpawnTime = absolute;
            this.spawnFrequency *= 1.02;
            this.factory.addEnemy(this.owner, this.model);
            this.sendMessage(MSG_ENEMY_CREATED);
        }
    }
}