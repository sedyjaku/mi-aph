import {ProjectileComponent} from "./ProjectileComponent";

export class EnemyProjectileComponent extends ProjectileComponent{
    onUpdate(delta, absolute) {
        this.owner.getPixiObj().position.y += delta * 0.05 * this.gameSpeed;
        super.onUpdate(delta, absolute);
    }
}