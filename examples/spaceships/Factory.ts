import Scene from "../../ts/engine/Scene";
import {Model, SpriteInfo} from "./Model";
import {
    ATTR_FACTORY,
    ATTR_MODEL,
    INTRO,
    FLAG_COLLIDABLE_BY_ENEMY,
    FLAG_COLLIDABLE_BY_PLAYER,
    FLAG_PROJECTILE_ENEMY,
    FLAG_PROJECTILE_PLAYER,
    TAG_BACKGROUND,
    TAG_ENEMY,
    TAG_LEVEL,
    TAG_LEVEL_ONE,
    TAG_PROJECTILE,
    TAG_SPACESHIP,
    TEXTURE_ENEMY,
    TEXTURE_SHOT_1,
    TEXTURE_SHOT_ENEMY,
    TEXTURE_SPACESHIP,
    TEXTURE_SPACESHIPS,
    TAG_GAME_OVER, TAG_LIVES, TAG_SCORE
} from "./Constants";
import {KeyInputComponent} from "../../ts/components/KeyInputComponent";
import PIXIObjectBuilder from "../../ts/engine/PIXIObjectBuilder";
import {SpaceshipInputController} from "./SpaceshipComponent";
import {PIXICmp} from "../../ts/engine/PIXIObject";
import {EnemyComponent} from "./EnemyComponent";
import {PlayerProjectileComponent} from "./PlayerProjectileComponent";
import {EnemyProjectileComponent} from "./EnemyProjectileComponent";
import {EnemySpawner} from "./EnemySpawner";
import {SoundComponent} from "./SoundComponent";
import {CollisionManager} from "./CollisionManager";
import {CollisionResolver} from "./CollisionResolver";
import {GenericComponent} from "../../ts/components/GenericComponent";
import {GameComponent} from "./GameComponent";
import {IntroComponent} from "./IntroComponent";

export class Factory {

    static globalScale = 1;

    initializeLevel(scene: Scene, model: Model) {
        //if game just started, show intro
        if(model.currentLevel == 0 ){
            this.addIntro(scene, model);
        }
        else {
            let builder = new PIXIObjectBuilder(scene);
            builder
                .withComponent(new EnemySpawner())
                .withComponent(new SoundComponent())
                .withComponent(new CollisionManager())
                .withComponent(new CollisionResolver())
                .build(scene.stage);
            this.addSpaceship(scene, model);
            scene.addGlobalComponent(new KeyInputComponent());
            scene.addGlobalComponent(new GameComponent());
        }
    }



    resetGame(scene: Scene, model: Model){
        model.isGameOver=false;
        let builder = new PIXIObjectBuilder(scene);
        scene.clearScene();
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);

        //add game over text
        let text = "GAME OVER";
        let gameOver = new PIXICmp.Text(TAG_GAME_OVER, text);
        gameOver.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        });
        gameOver.visible = false;
        builder.relativePos(0.5, 0.5).scale(Factory.globalScale).anchor(0.5, 0.5).build(gameOver, scene.stage);

        //add lives show component
        model.lives = model.maxLives;
        let lives = new PIXICmp.Text(TAG_LIVES);
        lives.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        });
        builder.relativePos(0, 1.01).scale(Factory.globalScale).anchor(0, 1)
            .withComponent(new GenericComponent("LivesComponent").doOnUpdate((cmp, delta, absolute) => {
                let lives = "LIVES: " + Math.max(0, model.lives);
                let text = <PIXI.Text>cmp.owner.getPixiObj();
                text.text = lives;
            }))
            .build(lives, scene.stage);

        //add score show component
        let score = new PIXICmp.Text(TAG_SCORE);
        score.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        });
        builder.relativePos(0.95 ,1.01).scale(Factory.globalScale).anchor(1, 1)
            .withComponent(new GenericComponent("ScoreComponent").doOnUpdate((cmp, delta, absolute) => {
                let score = "SCORE: " + model.score.toFixed(0);
                let text = <PIXI.Text>cmp.owner.getPixiObj();
                text.text = score;
            }))
            .build(score, scene.stage);

        //add level show component
        let level = new PIXICmp.Text(TAG_LEVEL);
        level.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        });
        builder.relativePos(0.6, 1.01).scale(Factory.globalScale).anchor(1, 1)
            .withComponent(new GenericComponent("LevelComponent").doOnUpdate((cmp, delta, absolute) => {
                let level = "LEVEL: " + model.currentLevel.toFixed(0);
                let text = <PIXI.Text>cmp.owner.getPixiObj();
                text.text = level;
            }))
            .build(level, scene.stage);

        this.initializeLevel(scene, model);
    }

    /**
     *  adds intro
     */
    addIntro(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        // stage components
        builder
            .withComponent(new SoundComponent())
            .withComponent(new IntroComponent())
            .build(scene.stage)

        // title
        builder
            .relativePos(0.5, 0.25)
            .anchor(0.5)
            .scale(Factory.globalScale)
            .build(new PIXICmp.Sprite(TAG_LEVEL_ONE, this.createIntro(INTRO)), scene.stage);

    }

    /**
     *  adds player spaceship
     */
    public addSpaceship(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        builder
            .scale(Factory.globalScale)
            .withFlag(FLAG_COLLIDABLE_BY_ENEMY)
            .localPos(15,20)
            .withComponent(new SpaceshipInputController())
            .build(new PIXICmp.Sprite(TAG_SPACESHIP, this.createTexture(model.getSpriteInfo(TEXTURE_SPACESHIP))), scene.stage);
    }

    // loads intro texture
    private createIntro(name: string): PIXI.Texture {
        let texture = PIXI.Texture.fromImage(name);
        texture = texture.clone();
        return texture;
    }

    // loads texture from SpriteInfo entity
    private createTexture(spriteInfo: SpriteInfo, index: number = 0): PIXI.Texture {
        let texture = PIXI.Texture.fromImage(TEXTURE_SPACESHIPS);
        texture = texture.clone();
        texture.frame = new PIXI.Rectangle(spriteInfo.offsetX + spriteInfo.width * index, spriteInfo.offsetY, spriteInfo.width, spriteInfo.height);
        return texture;
    }
    createProjectile(spaceship: PIXICmp.ComponentObject, model: Model) {
        let rootObject = spaceship.getScene().stage;
        let spaceshipPixi = spaceship.getPixiObj();
        let height = spaceshipPixi.getBounds().height;
        let width = spaceshipPixi.getBounds().width;
        let spaceshipGlobalPos = spaceshipPixi.toGlobal(new PIXI.Point(0, 0));

        let createdTexture = this.createTexture(model.getSpriteInfo(TEXTURE_SHOT_1));

        // we need the projectile to be at the same location as the cannon with current rotation
        new PIXIObjectBuilder(spaceship.getScene())
            .globalPos(spaceshipGlobalPos.x + width / 2 - 0.5, spaceshipGlobalPos.y - height / 2 + 0.5)
            .scale(Factory.globalScale)
            .withFlag(FLAG_PROJECTILE_PLAYER)
            .withComponent(new PlayerProjectileComponent())
            .build(new PIXICmp.Sprite(TAG_PROJECTILE, createdTexture), rootObject);
    }


    // adds  enemy entity
    addEnemy(owner: PIXICmp.ComponentObject, model: Model) {
        let root = owner.getScene().stage;

        let posX = Math.random() * (model.enemySpawnMinX + model.enemySpawnMaxX);
        let posY =  + model.enemySpawnY;

        new PIXIObjectBuilder(owner.getScene())
            .withFlag(FLAG_COLLIDABLE_BY_PLAYER)
            .withFlag(FLAG_PROJECTILE_ENEMY)
            .withComponent(new EnemyComponent())
            .relativePos(posX, posY)
            .anchor(0.5, 0.5)
            .scale(Factory.globalScale)
            .build(new PIXICmp.Sprite(TAG_ENEMY, this.createTexture(model.getSpriteInfo(TEXTURE_ENEMY))), root);
    }

    // creates enemy shot
    createEnemyProjectile(enemy: PIXICmp.ComponentObject, model: Model) {
            let rootObject = enemy.getScene().stage;
            let enemyPixi = enemy.getPixiObj();
            let height = enemyPixi.getBounds().height;
            let width = enemyPixi.getBounds().width;
            let enemyGlobalPos = enemyPixi.toGlobal(new PIXI.Point(0, 0));

            let createdTexture = this.createTexture(model.getSpriteInfo(TEXTURE_SHOT_ENEMY));

            // we need the projectile to be at the same location as the cannon with current rotation
            new PIXIObjectBuilder(enemy.getScene())
                .globalPos(enemyGlobalPos.x + width/2 - 0.5 , enemyGlobalPos.y + height/2 - 0.5)
                .scale(Factory.globalScale)
                .withFlag(FLAG_PROJECTILE_ENEMY)
                .withComponent(new EnemyProjectileComponent())
                .build(new PIXICmp.Sprite(TAG_PROJECTILE, createdTexture), rootObject);
    }


}