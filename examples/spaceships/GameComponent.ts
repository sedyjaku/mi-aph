import {Factory} from './Factory';
import Component from '../../ts/engine/Component';
import {Model} from './Model';
import {PIXICmp} from '../../ts/engine/PIXIObject';
import {
    ATTR_FACTORY, ATTR_MODEL,
    MSG_GAME_FINISHED,
    MSG_GAME_OVER, MSG_LEVEL_FINISHED,
    MSG_NEXT_LEVEL, TAG_GAME_OVER
} from './Constants';
import Msg from '../../ts/engine/Msg';

/**
 * Component that orchestrates main logic of the game
 */
export class GameComponent extends Component {
    private model: Model;
    private factory: Factory;

    onInit() {
        this.subscribe(MSG_GAME_OVER, MSG_NEXT_LEVEL, MSG_LEVEL_FINISHED);

        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);

        if (this.model.currentLevel == 0) {
            this.model.currentLevel = 1;
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_GAME_OVER) {
            this.gameOver();
        } else if (msg.action == MSG_LEVEL_FINISHED) {
            this.finishLevel();
        }
    }


    /**
     * resets model to level 1 values and reloads models
     */
    protected gameOver() {
        // display title
        let gameOverObj = this.scene.findFirstObjectByTag(TAG_GAME_OVER);
        gameOverObj.getPixiObj().visible = true;
        this.model.isGameOver = true;
        // wait 5 seconds and reset the game
        this.model.currentLevel = 1;
        let level = this.model.levels[this.model.currentLevel - 1];
        this.model.fireRate = level.fireRate;
        this.model.enemyReward = level.enemyReward;
        this.model.scoreToNext = level.scoreToNext;
        this.model.lives = this.model.maxLives;
        this.model.score = 0;
        this.reset();
    }

    /**
     * finishes level and moves to another one
     */
    protected finishLevel() {
        // go to the next level
        if (this.model.currentLevel == this.model.maxLevel) {
            this.model.currentLevel = 0; // back to intro scene
            this.sendMessage(MSG_GAME_FINISHED);
        } else {
            this.model.currentLevel++;
            let level = this.model.levels[this.model.currentLevel - 1];
            this.model.fireRate = level.fireRate;
            this.model.enemyReward = level.enemyReward;
            this.model.scoreToNext = level.scoreToNext;
        }
    }

    /**
     * resets whole game
     */
    private reset() {
        this.scene.invokeWithDelay(5000, () => {
            this.factory.resetGame(this.scene, this.model);
        });
    }
}