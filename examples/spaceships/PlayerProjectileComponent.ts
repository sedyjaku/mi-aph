import {ProjectileComponent} from "./ProjectileComponent";

export class PlayerProjectileComponent extends ProjectileComponent{
    onUpdate(delta, absolute) {
        this.owner.getPixiObj().position.y -= delta * 0.05 * this.gameSpeed;
        super.onUpdate(delta, absolute);
    }
}