import Component from "../../ts/engine/Component";
import {Model} from "./Model";
import {ATTR_FACTORY, ATTR_MODEL, MSG_PROJECTILE_SHOT} from "./Constants";
import {
    KEY_DOWN, KEY_I,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_SPACE, KEY_U,
    KEY_UP, KEY_X,
    KeyInputComponent
} from "../../ts/components/KeyInputComponent";
import {Factory} from "./Factory";
import {checkTime} from "./Utilts";



const SPACESHIP_HOR_POS_MIN = 1;
const SPACESHIP_HOR_POS_MAX = 29;
const SPACESHIP_VER_POS_MIN = 1;
const SPACESHIP_VER_POS_MAX = 20;

export class SpaceshipController extends Component {
    private model: Model;
    private factory: Factory;
    spaceshipLastX = 0;
    spaceshipLastY = 0;
    private leftDirection = false;
    private upDirection = false;
    protected lastShot = 0;

    onInit(){
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
    }

    /**
     * Moves spaceship horizontally
     */
    moveHorizontal(left: boolean, delta: number){
        if(left){
            this.owner.getPixiObj().position.x -= this.model.spaceshipSpeed * delta;
        }
        else {
            this.owner.getPixiObj().position.x += this.model.spaceshipSpeed * delta;
        }

        //check boundaries
        this.owner.getPixiObj().position.x = Math.max(Math.min(this.owner.getPixiObj().position.x, SPACESHIP_HOR_POS_MAX), SPACESHIP_HOR_POS_MIN);
        this.leftDirection = (this.owner.getPixiObj().position.x - this.spaceshipLastX) < 0;
        this.spaceshipLastX = this.owner.getPixiObj().position.x;
    }

    /**
     * Moves spaceship vertically
     */
    moveVertical(up: boolean, delta: number){

        if(up){
            this.owner.getPixiObj().position.y -= this.model.spaceshipSpeed * delta;
        }
        else {
            this.owner.getPixiObj().position.y += this.model.spaceshipSpeed * delta;
        }

        //check boundaries
        this.owner.getPixiObj().position.y = Math.max(Math.min(this.owner.getPixiObj().position.y, SPACESHIP_VER_POS_MAX), SPACESHIP_VER_POS_MIN);
        this.upDirection = (this.owner.getPixiObj().position.y - this.spaceshipLastY) < 0;
        this.spaceshipLastY = this.owner.getPixiObj().position.y;
    }

    /**
     * Fires a shot if reloaded (enough time passed between shots)
     */
    tryFire(absolute: number): boolean {
        if (checkTime(this.lastShot, absolute, this.model.fireRate)) {
            this.lastShot = absolute;
            this.factory.createProjectile(this.owner, this.model);
            this.sendMessage(MSG_PROJECTILE_SHOT);
            return true;
        } else {
            return false;
        }
    }

}



/**
 * Keyboard controller for spaceship
 */
export class SpaceshipInputController extends SpaceshipController {
    onUpdate(delta: number, absolute: number) {

        // get a global component
        let cmp = this.scene.stage.findComponentByClass(KeyInputComponent.name);
        let cmpKey = <KeyInputComponent><any>cmp;

        if (cmpKey.isKeyPressed(KEY_LEFT)) {
            this.moveHorizontal(true, delta);
        }

        if (cmpKey.isKeyPressed(KEY_RIGHT)) {
            this.moveHorizontal(false, delta);
        }

        if (cmpKey.isKeyPressed(KEY_UP)) {
            this.moveVertical(true, delta);
        }
        if (cmpKey.isKeyPressed(KEY_DOWN)) {
            this.moveVertical(false, delta);
        }
        if (cmpKey.isKeyPressed(KEY_X)) {
            this.tryFire(absolute);
        }
        if (cmpKey.isKeyPressed(KEY_SPACE)) {
            this.tryFire(absolute);
        }
    }
}