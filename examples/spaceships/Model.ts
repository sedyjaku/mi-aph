import Vec2 from '../../ts/utils/Vec2';
import { PIXICmp } from '../../ts/engine/PIXIObject';

export class Brick {
    type: number;
    position: Vec2;
}

// number of columns of each level
export const COLUMNS_NUM = 11;

/**
 * Entity that stores metadata about each sprite as loaded from JSON file
 */
export class SpriteInfo {

    constructor(name: string, offsetX: number, offsetY: number, width: number, height: number, frames: number) {
        this.name = name;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.frames = frames;
    }

    name: string;
    offsetX: number;
    offsetY: number;
    width: number;
    height: number;
    frames: number;
}

export class LevelInfo {
    constructor(name: string, fireRate: number, enemyReward: number, scoreToNext:number){
        this.name = name;
        this.fireRate = fireRate;
        this.enemyReward = enemyReward;
        this.scoreToNext = scoreToNext;
    }
    name: string;
    fireRate: number;
    enemyReward: number;
    scoreToNext: number;
}


export class Model {

    // metadata of all sprites as loaded from JSON file
    sprites: Array<SpriteInfo>;
    // 2D arrays of levels as loaded from JSON file
    levels: Array<LevelInfo>;
    spaceshipSpeed = 0.02;
    fireRate;

    enemiesCreated = 0;
    maxEnemies;
    enemySpawnFrequency;
    enemySpawnMaxX;
    enemySpawnMinX;
    enemySpawnY;
    enemyReward;
    score = 0;
    lives;
    maxLives;
    currentLevel = 0;
    scoreToNext;
    maxLevel;

    isGameOver=false;



    /**
     * Loads model from a JSON structure
     */
    loadModel(data: any) {
        this.sprites = new Array<SpriteInfo>();
        this.levels = new Array<LevelInfo>();
        this.enemySpawnFrequency = data.enemy_spawn_frequency;
        this.enemySpawnMaxX = data.enemy_spawn_min_x;
        this.enemySpawnMinX = data.enemy_spawn_max_x;
        this.enemySpawnY = data.enemy_spawn_y;
        this.maxLives = data.max_lives;
        this.lives = this.maxLives;

        for(let spr of data.sprites){
            this.sprites.push(new SpriteInfo(spr.name, spr.offset_px_x, spr.offset_px_y, spr.sprite_width, spr.sprite_height, spr.frames));
        }
        for(let lvl of data.levels){
            this.levels.push(new LevelInfo(lvl.name, lvl.fire_rate, lvl.enemy_reward, lvl.score_to_next));
        }
        let level = this.levels[0];
        this.fireRate = level.fireRate;
        this.enemyReward = level.enemyReward;
        this.scoreToNext = level.scoreToNext;
        this.maxLevel = this.levels.length

    }

    getSpriteInfo(name: string): SpriteInfo {
        for (let spr of this.sprites) {
            if (spr.name == name) return spr;
        }
        return null;
    }
}
