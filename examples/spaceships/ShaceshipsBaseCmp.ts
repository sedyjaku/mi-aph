
import {Factory} from "./Factory";
import {Model} from "./Model";
import {ATTR_FACTORY, ATTR_MODEL} from "./Constants";
import Component from "../../ts/engine/Component";


export class SpaceshipsBaseCmp extends Component {
    model: Model;
    factory: Factory;

    onInit() {
        this.model = this.scene.getGlobalAttribute<Model>(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute<Factory>(ATTR_FACTORY);
    }
}