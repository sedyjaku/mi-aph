
import { Point } from 'pixi.js';
import Component from "../../ts/engine/Component";

/**
 * Movement logic for projectile
 */
export class ProjectileComponent extends Component {

    protected gameSpeed: number;

    constructor(gameSpeed: number = 1) {
        super();
        this.gameSpeed = gameSpeed;
    }

    onUpdate(delta, absolute) {
        // check boundaries
        let globalPos = this.owner.getPixiObj().toGlobal(new Point(0, 0));
        if (globalPos.x < 0 || globalPos.x > this.scene.app.screen.width || globalPos.y < 0 || globalPos.y > this.scene.app.screen.height) {
            this.owner.remove();
        }
    }
}