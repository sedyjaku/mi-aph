/**
 * Simple logic for copters
 */
import {SpaceshipsBaseCmp} from "./ShaceshipsBaseCmp";
import {checkTime} from "./Utilts";
import {MSG_ENEMY_PROJECTILE_SHOT, MSG_LEVEL_FINISHED, MSG_UNIT_KILLED, TAG_ENEMY} from "./Constants";
import {Point} from "pixi.js";
import Msg from "../../ts/engine/Msg";


export class EnemyComponent extends SpaceshipsBaseCmp {
    lastShootTime = 0;
    shootFrequency = 0.3;


    protected gameSpeed: number;

    constructor(gameSpeed: number = 1) {
        super();
        this.gameSpeed = gameSpeed;
    }
    onInit() {
        super.onInit();
        this.subscribe(MSG_LEVEL_FINISHED);
    }

    /**
     * Enemy component moves down and shoots
     */
    onUpdate(delta: number, absolute: number) {
        this.owner.getPixiObj().position.y += delta * 0.01 * this.gameSpeed;

        // check boundaries
        let globalPos = this.owner.getPixiObj().toGlobal(new Point(0, 0));
        if (globalPos.x < 0 || globalPos.x > this.scene.app.screen.width || globalPos.y < 0 || globalPos.y > this.scene.app.screen.height) {
            this.owner.remove();
        }
        //shoot at a certain frequency
        if (checkTime(this.lastShootTime, absolute, this.shootFrequency)) {
            this.lastShootTime = absolute;
            // copter bounding bo
            this.factory.createEnemyProjectile(this.owner, this.model);
            this.sendMessage(MSG_ENEMY_PROJECTILE_SHOT);
        }
    }
}