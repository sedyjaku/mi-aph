import Component from "../../ts/engine/Component";
import {Model} from "./Model";
import {Factory} from "./Factory";
import {ATTR_FACTORY, ATTR_MODEL} from "./Constants";
import {MSG_GAME_STARTED} from "./Constants";

export class IntroComponent extends Component {
    private model: Model;
    private factory: Factory;

    onInit() {
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.sendMessage(MSG_GAME_STARTED);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);

        this.scene.invokeWithDelay(5000, () => {
            this.model.currentLevel = 1; // set the first level and reset the game
            this.factory.resetGame(this.scene, this.model);
        });
    }
}