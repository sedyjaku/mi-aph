import {PixiRunner} from "../../ts/PixiRunner";
import {
    SCENE_HEIGHT,
    SPRITES_RESOLUTION_HEIGHT,
    DATA_JSON,
    TEXTURE_SPACESHIPS,
    SOUND_FIRE, SOUND_GAMEOVER, SOUND_KILL, INTRO
} from "../spaceships/Constants";
import {Factory} from "../spaceships/Factory";
import {Model} from "../spaceships/Model";

class Spaceships {
    engine: PixiRunner;

    constructor(){
        this.engine = new PixiRunner();

        let canvas = (document.getElementById("gameCanvas") as HTMLCanvasElement);


        let screenHeight = canvas.height;

        // calculate ratio between intended resolution (here 400px of height) and real resolution
        // - this will set appropriate scale
        let gameScale = SPRITES_RESOLUTION_HEIGHT / screenHeight;
        // scale the scene to 25 units if height
        let resolution = screenHeight / SCENE_HEIGHT * gameScale;
        this.engine.init(canvas, resolution / gameScale);
        Factory.globalScale = 1 / resolution;
        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_SPACESHIPS, 'static/spaceships/sprites.png')
            .add(INTRO, 'static/spaceships/intro.png')
            .add(SOUND_FIRE, 'static/spaceships/fire.mp3')
            .add(SOUND_GAMEOVER, 'static/spaceships/gameover.mp3')
            .add(SOUND_KILL, 'static/spaceships/kill.mp3')
            .add(DATA_JSON, 'static/spaceships/data.json')
            .load(() => this.onAssetsLoaded());
    }


    onAssetsLoaded() {
        // init factory and model
        let factory = new Factory();
        let model = new Model();
        model.loadModel(PIXI.loader.resources[DATA_JSON].data);
        factory.resetGame(this.engine.scene, model);
    }
}

export var spaceships = new Spaceships();